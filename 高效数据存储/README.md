# 高效数据存储
## MySQL 深度优化
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/MySQL%20%E6%B7%B1%E5%BA%A6%E4%BC%98%E5%8C%96)
## Redis
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/Redis)
## elasticSearch
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/elasticSearch)
## mongodb
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/mongodb)