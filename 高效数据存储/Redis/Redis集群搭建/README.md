### Redis集群的搭建
#### redis-cluster
1. redis-cluster架构图
![redis-cluster架构图](https://images.gitee.com/uploads/images/2021/0909/182936_0df18163_9357316.png "redis-cluster架构图.png")
2. redis-cluster投票:容错
![redis-cluster投票](https://images.gitee.com/uploads/images/2021/0909/182952_a58b4d5c_9357316.png "redis-cluster投票.png")<br>
架构细节:<br>
(1)所有的redis节点彼此互联(PING-PONG机制) <br>
(2)节点的fail是通过集群中超过半数的节点检测失效时才生效 <br>
(3)集群中有一个节点fail，则整个集群都会fail <br>
(4)redis-cluster把所有的物理节点映射到[0-16383]slot上,cluster 负责维护node<->slot<->value <br>
> Redis 集群中内置了 16384 个哈希槽，当需要在 Redis 集群中放置一个 key-value 时，<br>
> redis 先对 key 使用 crc16 算法算出一个结果，然后把结果对 16384 求余数，<br>
> 这样每个 key 都会对应一个编号在 0-16383 之间的哈希槽，<br>
> redis 会根据节点数量大致均等的将哈希槽映射到不同的节点 <br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/183352_b5809b49_9357316.png "屏幕截图.png")
#### Redis集群搭建
Redis集群中至少应该有三个节点。要保证集群的高可用，需要每个节点有一个备份机。<br>
Redis集群至少需要6台服务器。<br>
搭建伪分布式。可以使用一台虚拟机运行6个redis实例。修改redis的端口号7001-7006<br>
##### Windows
###### 1. 配置6个redis实例
> 把 redis 解压后，再复制出 5 份，配置 三主三从集群<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/183605_738ea67d_9357316.png "屏幕截图.png")<br>
依次打开6个redis实例目录下的redis.windows.conf文件，修改里面的端口号，以及集群支持配置。<br>
```
配置端口号：
port:7001
配置支持集群：
cluster-enabled yes
cluster-config-file nodes-7001.conf
cluster-node-timeout 15000
appendonly yes
cluster-enabled ：不为yes那么在使用JedisCluster集群代码获取的时候，会报错。
cluster-node-timeout： 调整为 15000，那么在创建集群的时候，不会超时。
cluster-config-file nodes-7001.conf：是为该节点的配置信息，这里使用 nodes-端口.conf命名方法。服务启动后会在目录生成该文件。
```
###### 2. 安装6个redis实例到系统服务
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/183919_478e6488_9357316.png "屏幕截图.png")
###### 3. 安装Ruby
redis的集群使用ruby脚本编写，所以系统需要有Ruby环境 <br>
地址:`http://rubyinstaller.org/downloads/` <br>
勾选下面三个不用配置环境变量 <br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/183909_7c1e65e5_9357316.png "屏幕截图.png")
###### 4. 安装Redis的Ruby驱动redis-xxxx.gem
下载地址: `https://rubygems.org/pages/download`<br>
下载后解压，当前目录切换到解压目录中，如 D:\rubygems-2.7.7 然后在命令行执行 `ruby setup.rb`<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184109_9303684e_9357316.png "屏幕截图.png")<br>
然后GEM 安装 Redis ：切换到redis安装目录，需要在命令行中，执行 `gem install redis`<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184128_c1b0350e_9357316.png "屏幕截图.png")<br>
###### 5. 安装集群脚本redis-trib
下载：`https://raw.githubusercontent.com/antirez/redis/unstable/src/redis-trib.rb` <br>
打开该链接如果没有下载，而是打开一个页面，那么将该页面保存为redis-trib.rb，<br>
建议保存到一个Redis的目录下，例如放到7001目录下 <br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184154_f9e95b31_9357316.png "屏幕截图.png") <br>
执行集群构建脚本命令：<br>
`redis-trib.rb create --replicas 1 127.0.0.1:7001 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.0.1:7005 127.0.0.1:7006` <br>
> `--replicas 1` 表示每个主数据库拥有从数据库个数为1。<br>
> master节点不能少于3个，所以我们用了6个redis <br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184215_4235f737_9357316.png "屏幕截图.png")
##### 测试
使用Redis客户端Redis-cli.exe来查看数据记录数，以及集群相关信息 <br>
> 命令 `redis-cli –c –h ”地址” –p "端口号"` c 表示集群 <br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/185309_2ede7a6b_9357316.png "屏幕截图.png")<br>
> `cluster info`：查看集群信息<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/185320_2535b8b8_9357316.png "屏幕截图.png")<br>
> `cluster nodes`：查看集群众的节点<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/185333_ed1b0baf_9357316.png "屏幕截图.png")<br>

### Docker redis集群搭建
#### Linux
[参考链接](https://zhuanlan.zhihu.com/p/229103533)
##### 编写Redis配置文件
###### 1.创建目录及文件
分别在 192.168.10.10 和 192.168.10.11 两台机器上执行以下操作。
```
# 创建目录
mkdir -p /usr/local/docker-redis/redis-cluster
# 切换至指定目录
cd /usr/local/docker-redis/redis-cluster/
# 编写 redis-cluster.tmpl 文件
vi redis-cluster.tmpl
```
###### 2.编写配置文件
> 192.168.10.10 机器的 redis-cluster.tmpl 文件内容如下：
```
port ${PORT}
requirepass 1234
masterauth 1234
protected-mode no
daemonize no
appendonly yes
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 15000
cluster-announce-ip 192.168.10.10
cluster-announce-port ${PORT}
cluster-announce-bus-port 1${PORT}
```
> 192.168.10.11 机器的 redis-cluster.tmpl 文件内容如下：
```
port ${PORT}
requirepass 1234
masterauth 1234
protected-mode no
daemonize no
appendonly yes
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 15000
cluster-announce-ip 192.168.10.11
cluster-announce-port ${PORT}
cluster-announce-bus-port 1${PORT}
```
- port：节点端口；
- requirepass：添加访问认证；
- masterauth：如果主节点开启了访问认证，从节点访问主节点需要认证；
- protected-mode：保护模式，默认值 yes，即开启。开启保护模式以后，需配置 bind ip 或者设置访问密码；关闭保护模式，外部网络可以直接访问；
- daemonize：是否以守护线程的方式启动（后台启动），默认 no；
- appendonly：是否开启 AOF 持久化模式，默认 no；
- cluster-enabled：是否开启集群模式，默认 no；
- cluster-config-file：集群节点信息文件；
- cluster-node-timeout：集群节点连接超时时间；
- cluster-announce-ip：集群节点 IP，填写宿主机的 IP；
- cluster-announce-port：集群节点映射端口；
- cluster-announce-bus-port：集群节点总线端口。<br>
> 每个 Redis 集群节点都需要打开两个 TCP 连接。<br>
> 一个用于为客户端提供服务的正常 Redis TCP 端口，<br>
> 例如 6379。还有一个基于 6379 端口加 10000 的端口，比如 16379。<br>
> 第二个端口用于集群总线，这是一个使用二进制协议的节点到节点通信通道。<br>
> 节点使用集群总线进行故障检测、配置更新、故障转移授权等等。<br>
> 客户端永远不要尝试与集群总线端口通信，与正常的 Redis 命令端口通信即可，<br>
> 但是请确保防火墙中的这两个端口都已经打开，否则 Redis 集群节点将无法通信。<br>
在`192.168.10.10`机器的`redis-cluster`目录下执行以下命令：
```
for port in `seq 6371 6373`; do \
  mkdir -p ${port}/conf \
  && PORT=${port} envsubst < redis-cluster.tmpl > ${port}/conf/redis.conf \
  && mkdir -p ${port}/data;\
done
```
在 `192.168.10.11`机器的`redis-cluster`目录下执行以下命令：
```
for port in `seq 6374 6376`; do \
  mkdir -p ${port}/conf \
  && PORT=${port} envsubst < redis-cluster.tmpl > ${port}/conf/redis.conf \
  && mkdir -p ${port}/data;\
done
```
> 上面两段 shell for 语句，意思就是循环创建 6371 ~ 6376 相关的目录及文件。<br>
在`192.168.10.10`和`192.168.10.11`机器执行查看命令结果如下，如果没有 tree 命令先安装 `yum install -y tree` 。<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0911/094445_2a47a61a_9357316.png "屏幕截图.png")<br>
##### 编写Docker Compose模板文件
> 在 192.168.10.10 机器的 /usr/local/docker-redis 目录下创建 docker-compose.yml 文件并编辑。<br>
```go
# 描述 Compose 文件的版本信息
version: "3.8"

# 定义服务，可以多个
services:
  redis-6371: # 服务名称
    image: redis # 创建容器时所需的镜像
    container_name: redis-6371 # 容器名称
    restart: always # 容器总是重新启动
    network_mode: "host" # host 网络模式
    volumes: # 数据卷，目录挂载
      - /usr/local/docker-redis/redis-cluster/6371/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6371/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf # 覆盖容器启动后默认执行的命令

  redis-6372:
    image: redis
    container_name: redis-6372
    network_mode: "host"
    volumes:
      - /usr/local/docker-redis/redis-cluster/6372/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6372/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf

  redis-6373:
    image: redis
    container_name: redis-6373
    network_mode: "host"
    volumes:
      - /usr/local/docker-redis/redis-cluster/6373/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6373/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf
```
> 在 192.168.10.11 机器的 /usr/local/docker-redis 目录下创建 docker-compose.yml 文件并编辑。
```go
# 描述 Compose 文件的版本信息
version: "3.8"

# 定义服务，可以多个
services:
  redis-6374: # 服务名称
    image: redis # 创建容器时所需的镜像
    container_name: redis-6374 # 容器名称
    restart: always # 容器总是重新启动
    network_mode: "host" # host 网络模式
    volumes: # 数据卷，目录挂载
      - /usr/local/docker-redis/redis-cluster/6374/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6374/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf # 覆盖容器启动后默认执行的命令

  redis-6375:
    image: redis
    container_name: redis-6375
    network_mode: "host"
    volumes:
      - /usr/local/docker-redis/redis-cluster/6375/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6375/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf

  redis-6376:
    image: redis
    container_name: redis-6376
    network_mode: "host"
    volumes:
      - /usr/local/docker-redis/redis-cluster/6376/conf/redis.conf:/usr/local/etc/redis/redis.conf
      - /usr/local/docker-redis/redis-cluster/6376/data:/data
    command: redis-server /usr/local/etc/redis/redis.conf
```
##### 创建并启动所有服务容器
分别在 192.168.10.10 和 192.168.10.11 机器的 /usr/local/docker-redis 目录下执行以下命令：<br>
`docker-compose up -d`<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0911/095627_04f3c8fb_9357316.png "屏幕截图.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0911/095556_25d6b809_9357316.png "屏幕截图.png")
##### 创建Redis Cluster集群
> 请先确保你的两台机器可以互相通信，然后随便进入一个容器节点，并进入 /usr/local/bin/ 目录：
```go
# 进入容器
docker exec -it redis-6371 bash
# 切换至指定目录
cd /usr/local/bin/
```
接下来我们就可以通过以下命令实现 Redis Cluster 集群的创建<br>
`redis-cli -a 1234 --cluster create 192.168.10.10:6371 192.168.10.10:6372 192.168.10.10:6373 192.168.10.11:6374 192.168.10.11:6375 192.168.10.11:6376 --cluster-replicas 1`<br>
出现选择提示信息，输入 yes，结果如下所示：<br>
![RedisCluster集群的创建](https://images.gitee.com/uploads/images/2021/0911/095932_35a2ffb2_9357316.png "屏幕截图.png")<br>
![集群创建成功](https://images.gitee.com/uploads/images/2021/0911/100041_8f4ddb85_9357316.png "屏幕截图.png")<br>
> 至此一个高可用的 Redis Cluster 集群搭建完成，<br>
> 该集群中包含 6 个 Redis 节点，3 主 3 从。<br>
> 三个主节点会分配槽，处理客户端的命令请求，而从节点可用在主节点故障后，顶替主节点。<br>
##### 删除集群环境
`docker-compose down`
##### 查看集群状态
> 我们先进入容器，然后通过一些集群常用的命令查看一下集群的状态。
```go
# 进入容器
docker exec -it redis-6371 bash
# 切换至指定目录
cd /usr/local/bin/
# 检查集群状态
redis-cli -a 1234 --cluster check 192.168.10.11:6375
```
###### 查看集群信息和节点信息
```go
# 连接至集群某个节点
redis-cli -c -a 1234 -h 192.168.10.11 -p 6376
# 查看集群信息
cluster info
# 查看集群结点信息
cluster nodes
```