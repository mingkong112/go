# docker基本使用
### 容器技术
> Docker是一个用于开发，交付和运行应用程序的开发平台
#### Docker与虚拟机
> 与传统虚拟机相比，Docker启动速度快，占用体积小<br>
![Docker与虚拟机](https://images.gitee.com/uploads/images/2021/0827/150936_7ab54882_9357316.png "屏幕截图.png")
### 安装
[官方文档](https://docs.docker.com/get-docker/)<br>
[daocloud镜像](http://get.daocloud.io/)
#### Windows迁移默认路径
1. 将两个文件夹转移到指定路径<br>
`C:\Program Files\Docker`
`C:\ProgramData\DockerDesktop`
2. WIN+R 输入 regedit 打开注册表<br>
`HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services`
中查找 `com.docker.service`
将ImagePath 修改为新路径
3. 修改环境变量为新路径<br>
4. 最后进入新目录.\Docker\Docker 双击 Docker Desktop.exe 启动docker服务<br>
如果失败 可参考
[win10使用WSL 2运行Docker Desktop，运行文件从C盘迁移到其他目录](https://blog.csdn.net/dafei1288/article/details/110102273)

#### docker.io
Ubuntu官方基于docker社区源码封装的版本<br>
docker.io 特点是将docker的依赖托管给主系统,只需要更新docker主程序即可<br>
```go
//安装
sudo apt-get install docker.io
//删除
sudo apt-get remove docker docker-engine docker.io containerd runc
```

#### docker-ce(社区版 免费)
> Docker放出来的社区版,仅维护源码<br>
> 特点是使用golang将依赖封装在一个包中<br>
> 也因此有一个致命缺陷-依赖<br>
> Docker本身依赖成百上千个第三方依赖<br>
> 理论上只要有一个依赖出问题就需要完全重新编译docker<br>
```go
# //Ubuntu
apt-get install docker-ce
# //CentOS
yum install docker-ce
# //安装 docker-ce 之前，要先卸载旧版本：
# //Ubuntu
apt-get remove docker docker-engine docker.io
# //CentOS
yum remove docker docker-common docker-selinux docker-engine 
```

#### docker-ee(企业版 收费)
Docker维护的商业版,主要有以下三个级别:
基本:用于认证基础架构的Docker平台,得到Docker Inc.的支持以及来自Docker Store的认证容器和插件<br>
标准:添加了高级映像和容器管理,LDAP / AD用户集成以及基于角色的访问控制.这些功能共同构成了Docker企业版<br>
高级:添加Docker安全扫描和连续漏洞监控<br>
```go
# //Ubuntu
apt-get install docker-engine
# //CentOS
yum install docker-engine
```

### 架构
![Docker架构](https://images.gitee.com/uploads/images/2021/0904/155823_011f554c_9357316.png "Docker架构.png")<br>
1. Client(客户端)
2. DOCKER_HOST(主机 docker引擎)
3. Registry(仓库)    
> Docker在运行时分为 docker引擎（服务端守护进程）和客户端工具<br>
> 日常使用的各种docker命令，就是在使用客户端工具与docker引擎进行交互<br>
[registry镜像](https://hub.docker.com/search?q=registry&type=image)<br>
### 技术
### 镜像(image)
阿里云镜像仓库
```go
  "registry-mirrors": [
    "https://o3trwnyj.mirror.aliyuncs.com"
  ]
```
[官方镜像版本查询地址](https://hub.docker.com/)
```go
docker search	查找
docker image pull	拉取镜像(默认拉取latest最新版)
docker image ls	获取本地镜像列表
docker image history	镜像历史构建信息
docker image inspect	镜像详细信息
docker image rm nginx:test1	删除
docker image prune	清理未使用镜像（生产环境小心使用）
docker image tag nginx:1.1 nginx:test1	容器内自定义版本(重命名)
docker image push	将镜像推送到镜像仓库
docker image save	将镜像导出到tar文件
docker image load 将tar文件加载镜像
docker image import	将容器文件系统tar文件导入镜像
docker image build	从dockerfile构建镜像
```
> eg:<br>
> 拉取镜像 默认拉取最后一个版本 <br>
![拉取nginx镜像](https://images.gitee.com/uploads/images/2021/0830/145133_4e1b976f_9357316.png "拉取nginx镜像.png")<br>
> 拉取指定版本<br>
![拉取指定版本](https://images.gitee.com/uploads/images/2021/0830/145300_f2e926b3_9357316.png "拉取指定版本.png")<br>

### 容器(container)
```go
docker container run	运行容器
docker creat	创建容器
docker container ls	查看运行过 运行中 的容器列表
docker container inspect	详细信息
docker container exec	进入容器
docker container start	开始
docker container stop	停止
docker container restart	重启
docker container kill	杀死
docker container logs    日志
docker container rm    删除
docker container prune	清理没有运行的
docker container top	查看资源占用
docker container cp	拷贝
docker container port	端口映射情况
docker container rename	重命名
docker container stats	查看容器资源使用
docker container export	导出容器文件系统到tar文件
docker container commit	容器提交为镜像
docker container update	容器更新配置
```
> **_eg:_** <br>
1. 启动容器<br>
![启动一个容器](https://images.gitee.com/uploads/images/2021/0830/151625_225c8a7d_9357316.png "启动一个容器.png")<br>
```
docker container run -itd --name NginxTest -p 9999:80 nginx:latest
-itd //启动交互式环境
--name //容器命名
-p 9999:80 //暴露端口:容器内端口
nginx:latest //镜像:版本号
```
> 查看容器列表<br>
![查看容器列表](https://images.gitee.com/uploads/images/2021/0830/151530_745a5a12_9357316.png "查看容器列表.png")<br>
> 查看容器端口<br>
![查看容器端口](https://images.gitee.com/uploads/images/2021/0830/151601_48eb74df_9357316.png "查看容器端口.png")<br>
2. 进入容器<br>
```
//进入容器
docker container exec -it NginxTest /bin/bash
//复制容器内文件到当前目录
docker container cp NginxTest:/usr/share/nginx/html/index.html .
//复制容器内文件到指定目录
docker container cp NginxTest:/usr/share/nginx/html/index.html w:\www
//复制当前目录下文件到容器内指定目录
docker container cp test.html NginxTest:/usr/share/nginx/html/
```
> 复制index.html文件index2.html 输出hello<br>
![管理容器](https://images.gitee.com/uploads/images/2021/0830/153036_a9ffc5fc_9357316.png "管理容器.png")<br>
![访问index2](https://images.gitee.com/uploads/images/2021/0830/152924_cfb89edb_9357316.png "访问index2.png")<br>

3. 退出容器<br>
![退出容器](https://images.gitee.com/uploads/images/2021/0830/154031_e1a521a6_9357316.png "退出容器.png")<br>

4. 停止容器<br>
> `ls`查看运行中的、`ls -a` 查看所有的<br>
![停止容器](https://images.gitee.com/uploads/images/2021/0830/155031_0eca9589_9357316.png "停止容器.png")<br>
### 挂载

1. 挂载到内存中
> 重启后容器消失
2. 本地磁盘
> 指定目录挂载 容器数据卷操作
> 启动nginx 将目录 D:\www\test 挂载到 /usr/share/nginx/html/ 目录下 在test目录修改内容
> 其他匿名挂载 具名挂载等
```
docker container run -itd --name NginxTest -v D:\www\test:/usr/share/nginx/html/ -p 9991:80 nginx:latest
```
3. 远程挂载
### 网络(network)
1. bridge 本地默认为网桥模式<br>
2. host 直接使用宿主机的ip和端口<br>
3. none 没有网络 需要自定义<br>
4. container <br>
> 创建容器时通过参数<br>
`--net container:<xxx>` 或 `--network container:<xxx>`<br>
已运行容器xxx的名称或id指定共享一个网络栈<br>
（两个容器之间使用 loaclhost 高效快速通信）<br>
5. link（已过时）

#### 自定义网络
保证容器中应用的安全性<br>
可以使用Docker DNS（默认的bridge中无法使用）<br>
1. 创建网络<br>
`docker network create custom_network`<br>
2. 查看网络<br>
`docker network ls`<br>
3. 连接网络 <br>
`docker network connect 网络名称 容器名称`<br>
4. 断开网络<br>
`docker network disconnect 网络名称 容器名称`<br>
5. 移除网络<br>
`docker network rm 网络名称`<br>

### dockerfile

### docker-compose
> Dockerfile文件方便定义一个单独的应用容器<br>
> Compose定义和运行多容器Docker应用程序<br>
地址：`https://github.com/docker/compose/releases`<br>
> 使用YAML文件来配置应用程序所需服务<br>
> 通过YAML配置文件创建并启动所有服务<br>

1.使用Dockerfile文件定义应用程序的环境
2.使用docker-compose.yml 文件定义构成应用程序的服务，这样它们可以在隔离环境中一起运行
3.执行 docker-compose up 命令来创建并启动所有服务<br>
 **_实例 安装_** 
`https://docs.docker.com/compose/`<br>

```
# 指定Compose文件版本信息 optional since v1.27.0
version: "3.9"  
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  nginx:	# 服务名称
    image: nginx	# 创建容器所需镜像
    container_name: mynginx	# 容器名称，默认为'工程名_服务条目名_序号'
    ports:	# 宿主机与容器的端口映射关系
      - "80:80"	# 宿主机端口:容器端口
    network:	# 配置容器连接的网络 引用顶级networks下的条目
      - nginx-net
# 定义网络：可多个。如不声明 默认创建一个名为"工程名称_default"的bridge网络
networks:
    nginx-net:	# 一个具体网络的条目名称
        name: nginx-net	# 网络名称，默认为"工程名称_网络条目名称"
        driver: bridge	# 网络模式，默认为bridge
volumes:
  logvolume01: {}
```
#### Redis集群搭建
[Compose 搭建 redis-cluster 集群环境](https://gitee.com/mingkong112/go/blob/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/Redis/Redis%E9%9B%86%E7%BE%A4%E6%90%AD%E5%BB%BA/README.md#docker-redis%E9%9B%86%E7%BE%A4%E6%90%AD%E5%BB%BA)

### 自建私有镜像仓库

#### 私有仓库镜像搭建

1.拉取私有仓库镜像
`docker pull registry`

2.修改 daemon.json 配置文件
```
vi /etc/docker/daemon.json
# 添加以下内容，用于让 Docker 信任私有仓库地址，保存退出。
{
    "insecure-registries": ["192.168.10.10:5000"]
}
# 重新加载某个服务的配置文件
sudo systemctl daemon-reload
# 重新启动 docker
sudo systemctl restart docker
```

3.创建私有仓库容器
```
docker run -di --name registry -p 5000:5000 -v /mydata/docker_registry:/var/lib/registry registry
```
-d：后台运行容器<br>
--name：为创建的容器命名<br>
-p：表示端口映射，前者是宿主机端口，后者是容器内的映射端口。可以使用多个 -p 做多个端口映射<br>
-v：将容器内 /var/lib/registry 目录下的数据挂载至宿主机 /mydata/docker_registry 目录下<br>
打开浏览器输入：http://192.168.10.10:5000/v2/_catalog 看到 {"repositories":[]} 表示私有仓库搭建成功并且内容为空<br>

4.推送镜像至私有仓库
```
docker tag hello-world:latest 192.168.10.10:5000/test-hello-world:1.0.0
docker push 192.168.10.10:5000/test-hello-world:1.0.0
```
浏览器:`http://192.168.10.10:5000/v2/_catalog`可以看到私有仓库中已上传的镜像

#### 私有仓库认证功能

1. 创建证书存储目录
```
mkdir -p  /www
```

2. 生成自签名证书
```
openssl req -newkey rsa:4096 -nodes -sha256 -keyout /home/registry/certs/domain.key -x509 -days 365 -out /home/registry/certs/domain.crt
```
>`openssl req`：创建证书签名请求等功能<br>
>`-newkey`：创建CSR证书签名文件和RSA私钥文件<br>
>`rsa:4096`：指定创建的RSA私钥长度为4096<br>
>`-nodes`：对私钥不进行加密<br>
>`-sha256`：使用SHA256算法<br>
>`-keyout`：创建的私钥文件名称及位置<br>
>`-x509`：自签发证书格式<br>
>`-days`：证书有效期<br>
>`-out`：指定CSR输出文件名称及位置<br>

**通过 openssl 生成自签名证书 运行命令以后**<br>
**其他信息可以随便填**<br>
`Common Name (eg, your name or your server's hostname) []: 192.168.10.10`<br>
**这里填写的是私有仓库的地址**<br>

3. 生成鉴权密码文件
```
# 创建存储鉴权密码文件目录
mkdir -p /home/registry/auth
# 如果没有 htpasswd 功能需要安装 httpd
yum install -y httpd
# 创建用户 root 和密码 123456 
htpasswd -Bbn root 123456 > /home/registry/auth/htpasswd
```
>htpasswd 是 apache http 的基本认证文件，使用 htpasswd 命令可以生成用户及密码文件。

4. 创建私有仓库容器
> Docker 官方提供了一个叫做 registry 的镜像用于搭建本地私有仓库使用。
```
//启动容器registry
docker run -di --name registry -p 5000:5000 \
   -v /mydata/docker_registry:/var/lib/registry \
   -v /home/registry/certs:/certs \
   -v /home/registry/auth:/auth \
   -e "REGISTRY_AUTH=htpasswd" \
   -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
   -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
   -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
   -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
   registry
```
>`-v` 目录挂载<br>
>`-e` 环境变量<br>
此时 推送镜像至私有仓库 会失败

5. 登录账号
```
docker login 192.168.10.10:5000
```
再次 push 镜像，可以成功推送。
`docker push 192.168.10.10:5000/test:0.0.1`

6. 退出账号
```
docker logout 192.168.10.10:5000
```
### 常见问题
#### Docker Desktop 
##### failed to start 
`error during connect: This error may indicate that the docker daemon is not running.:`<br>
```go
cd "C:\Program Files\Docker\Docker"
DockerCli.exe -SwitchDaemon
```
##### WSL 2 installation is incomplete.
[下载新的WSL 2包](https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-4---download-the-linux-kernel-update-package)
重启Docker即可