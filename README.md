# go notes
## go 基础
[go基础知识点](https://gitee.com/mingkong112/go/tree/master/go%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E7%82%B9)
## go 内核
### GPM模型
[go调度器 GPM模型](https://gitee.com/mingkong112/go/tree/master/go%E5%86%85%E6%A0%B8/GPM%E6%A8%A1%E5%9E%8B)

### 测试及性能
1. 基准测试
2. 压力测试
3. 子测试
4. 调试工具delve
5. go test工作机制
### 调试及go编译
1. go编译分析
2. 词法与语法分析
3. 类型检测
### 常见数据结构实现
1. 数组底层数据结构
2. slice底层数据结构
3. slice扩容策略分析
4. 内存对齐分析
5. map底层实现与线程安全分析
### 反射与接口
1. 反射三法则
2. 类型断言
3. 类型和值
4. 实现协议
5. 方法调用
### 并发编程
[go 并发编程](https://gitee.com/mingkong112/go/tree/master/go%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E7%82%B9/%E5%B9%B6%E5%8F%91%E7%BC%96%E7%A8%8B)
### 内存管理
1. go内存分配的分配机制
2. go内存分配的内部结构
3. go Gc(垃圾回收机制)介绍
4. 三色标记的实现原理
5. GC的流程
6. GC回收优化
7. 栈内存管理
8. 逃逸分析
## web 开发
### gin 框架
### beego 框架
### 框架源码分析
1. go构建框架思路
2. gin框架启动原理
3. gin路由注册解析
4. gin中间件实现原理
5. beego启动流程
6. beego请求解析原理
7. controller运行机制
8. beego-filter实现原理
### 自建框架 demo
1. 框架设计
2. 核心结构
3. 事件处理
4. route设计加载
5. 框架中间件设计
6. 视图渲染
7. 配置载入
8. 实践orm
## zinx 框架
> 基于Golang轻量级TCP并发服务器框架
## 高效数据存储
### MySQL 深度优化
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/MySQL%20%E6%B7%B1%E5%BA%A6%E4%BC%98%E5%8C%96)
1. MySQL存储引擎选择与注意事项
2. 解读MySQL共享及排他锁
3. MySQL事务及隔离级别
4. 底层Btree与B+tree机制
5. SQL执行计划深入详解
6. MySQL索引优化详解
7. 慢查询分析与SQL语句优化
### Redis
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/Redis)
1. 安装
2. String List Hash Set Zset类型使用场景
3. Lua+Redis开发
4. Redis慢操作优化
### elasticSearch 
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/elasticSearch)
1. 安装与应用
2. 与go对接
3. 索引结构
4. kibana搭建
5. elk日志收集方案
### mongodb
[详情链接](https://gitee.com/mingkong112/go/tree/master/%E9%AB%98%E6%95%88%E6%95%B0%E6%8D%AE%E5%AD%98%E5%82%A8/mongodb)
1. 使用场景
2. 基础操作(增删查改)
3. monogo 并发时注意事项
4. 安全设置及存储引擎分析指南
## 分布式架构设计方案
### 分布式结构原理
1. 大型互联网架构演进过程
2. 分布式架构设计原则
3. 分布式通信技术介绍
4. 分布式通信协议
5. 主流分布式架构设计详解
### 分布式缓存实战
1. 基于Redis实现分布式锁
2. 缓存架构（百万QPS高并发+99.99高可用+TB级海量数据）
3. 多级缓存
4. 缓存的一致性策略
5. 缓存雪崩解决方案(预防及限流)
6. 缓存穿透方案(缓存空数据及布隆过滤器)
### MySQL高可用
1. MySQL主从复制及读写分离高可用方案
2. MySQL+keepalived实现双主高可用方案
3. MySQL实现分库分表高性能解决方案
### Mycat
1. 简介及用途
2. 基于Mycat实现MySQL读写分离
3. 基于Mycat实现数据库切分
4. Mycat全局表、ER表、分片策略分析
### Nginx
1. 项目实现Nginx分流
2. 安装及基本使用
3. 进程模型及配置详解
4. location规则及rewrite解析
5. 动静分离
6. 反向代理
7. 跨域配置
8. 缓存配置及Gzip配置
9. https安全认证
10. lua脚本入门
11. LVS高可用
### Redis
1. Redis哨兵机制及底层机制
2. docker下redis主从配置、主从原理
3. sentinel原理、轮询实现分流
4. redis集群管理、客户端封装
5. 动态扩容、缩减集群节点
### Kafka
1. 快速安装与部署
2. 集群模式
3. Kafka的生产者与消费者
4. 高级特性
5. 处理请求内部机制
### RabbitMQ
1. linux下安装与配置
2. 消息发布与消息权衡
3. 消息的拒绝怎么解决
4. 控制队列与消息属性
5. 集群和与镜像队列实战
## 微服务
### 微服务简介
[微服务简介](https://gitee.com/mingkong112/go/tree/master/%E5%BE%AE%E6%9C%8D%E5%8A%A1)
### Docker 容器
[docker的基本使用](https://gitee.com/mingkong112/go/tree/master/%E5%BE%AE%E6%9C%8D%E5%8A%A1/docker%E7%9A%84%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8)
### consul 分布式指挥官
1. 安装及指令解析 
2. 客户端快速开发实战
3. 配置中心
4. 命名服务
5. consul集群选举、搭建
### rpc远程调用机制
1. RPC简介及原理介绍
2. 服务注册与发布
3. 动态代理
4. 序列号与反射
5. go语言实现rpc编程
6. rpc与protobuf结合使用
### Protobuf与gRPC
1. Protobuf 在go中的编程实现
2. Protobuf 协议语法与原理实现
3. gRPC介绍与安装
4. gRPC框架使用、调用
5. TLS验证和Token认证
6. 拦截器的使用
### 微服务框架
#### go-micro
1. 介绍
2. 创建微服务
3. 心跳机制
4. 事件驱动机制
5. Micro负载均衡组件-Selector
6. RESTful Api设计
7. go-micro 网关设计
#### go-zero
### k8s(kubernetes)
1. 简介及安装配置
2. 核心原理
3. 集群管理方案
### 自动化部署
1. jenkins自动化部署
2. jenkins集成
3. git+jenkins发布项目
### 参考网站
https://www.liwenzhou.com/
https://space.bilibili.com/1394739229
https://space.bilibili.com/373073810/video