## 线程，进程协程

## 并发与并行

并发：同一时间段内执行多个任务（你在用微信和两个女朋友聊天）。

并行：同一时刻执行多个任务（你和你朋友都在用微信和女朋友聊天）。

Go语言的并发通过goroutine实现。
goroutine类似于线程，属于 **用户态的线程** ，我们可以根据需要创建成千上万个goroutine并发工作。
goroutine是由Go语言的运行时（runtime）调度完成，而线程是由操作系统调度完成。

Go语言还提供channel在多个goroutine间进行通信。
goroutine和channel是 Go 语言秉承的 CSP（Communicating Sequential Process）并发模式的重要实现基础。

## 协程(Goroutine)

> `Goroutine 是与其他函数或方法同时运行的函数或方法。 `
>
> `Goroutines 可以被认为是轻量级的线程。 `
>
> `与线程相比，创建 Goroutine 的开销很小。 `
>
> `Go应用程序同时运行数千个 Goroutine 是非常常见的做法。`
>

### goroutine 运用

```go
var wg sync.WaitGroup 

func hello(i int) {
	defer wg.Done() // goroutine结束就登记-1
	fmt.Println("Hello Goroutine!", i)
}
func main() { //主goroutine执行main函数
	runtime.GOMAXPROCS(1) // 占用cpu核心数 限制同时执行Go代码的操作系统线程数为 1
	wg.Add(2000) // 计数牌 启动一个子goroutine就登记+1
	for i := 0; i < 1000; i++ {

		go hello(i) //开启一个goroutine执行这个hello函数
		//go func(){
		//	fmt.Println("Hello Goroutine!", i)
		//	wg.Done()
		//} //闭包错误
		go func(i int){ //开启一个goroutine执行这个匿名函数 
			fmt.Println("Hello Goroutine!", i)
			wg.Done()
		}(i) //闭包处理
	}
	//time.sleep(time.Second) //等待一秒 不推荐使用
	wg.Wait() // 阻塞，等待所有子goroutine都执行完
}
```

#### **sync.WaitGroup的使用场景**

限制goroutine并行数。

程序中需要并发，需要创建多个goroutine，并且一定要等这些并发全部完成后才继续接下来的程序执行．

WaitGroup的特点是Wait()可以用来阻塞直到队列中的所有任务都完成时才解除阻塞，而不需要sleep一个固定的时间来等待．但是其缺点是无法指定固定的goroutine数目（也就是协程池功能）．

#### 闭包错误引用同一个变量问题怎么处理

在每轮迭代中生成一个局部变量 i 。如果没有 i := i 这行，将会打印同一个变量。

```go
func main() {
    for i := 0; i < 5; i++ {
        i := i
        defer func() {
            println(i)
        }()
    }
}
```

或者是通过函数参数传入 i 。

```go
func main() {
    for i := 0; i < 5; i++ {
        defer func(i int) {
            println(i)
        }(i)
    }
}
```

#### Go 可以限制运行时操作系统线程的数量吗？

> The **GOMAXPROCS** variable limits the number of operating system threads that can execute user-level Go code simultaneously. There is no limit to the number of threads that can be blocked in system calls on behalf of Go code; those do not count against the GOMAXPROCS limit.
>

> 从官方文档的解释可以看到，`GOMAXPROCS` 限制的是同时执行用户态 Go 代码的操作系统线程的数量，但是对于被系统调用阻塞的线程数量是没有限制的。
>
> `GOMAXPROCS` 的默认值等于 CPU 的逻辑核数，同一时间，一个核只能绑定一个线程，然后运行被调度的协程。
>
> 因此对于 CPU 密集型的任务，若该值过大，例如设置为 CPU 逻辑核数的 2 倍，会增加线程切换的开销，降低性能。对于 I/O 密集型应用，适当地调大该值，可以提高 I/O 吞吐率。
>

Go运行时的调度器使用`GOMAXPROCS`参数来确定需要使用多少个OS线程来同时执行Go代码。默认值是机器上的CPU核心数。例如在一个8核心的机器上，调度器会把Go代码同时调度到8个OS线程上（GOMAXPROCS是m:n调度中的n）。

Go语言中可以通过`runtime.GOMAXPROCS()`函数设置当前程序并发时占用的CPU逻辑核心数。

Go1.5版本之前，默认使用的是单核心执行。Go1.5版本之后，默认使用全部的CPU逻辑核心数。

可以使用环境变量 `GOMAXPROCS` 或 `runtime.GOMAXPROCS(num int)` 设置，例如：

```go
runtime.GOMAXPROCS(1) // 限制同时执行Go代码的操作系统线程数为 1
```

Go语言中的操作系统线程和goroutine的关系：

1. 一个操作系统线程对应用户态多个goroutine。
2. go程序可以同时使用多个操作系统线程。
3. goroutine和OS线程是多对多的关系，即m:n（m个goroutine分配到n个操作系统线程上执行）

## 线程(Channel)

1. `channel`是一种类型，一种引用类型。声明通道类型的格式如下：

```go
var 变量 chan 元素类型
```

通道是引用类型，通道类型的空值是`nil`。

```go
var ch chan int fmt.Println(ch) // <nil> 
```

2. 声明的通道后需要使用`make`函数初始化之后才能使用。

创建channel的格式如下：

```go
make(chan 元素类型, [缓冲通道大小]) 
```

channel的缓冲大小是可选的，可以不填。

举几个例子：

```go
ch4 := make(chan int) ch5 := make(chan bool) ch6 := make(chan []int)
```

3. 通道有发送（send）、接收(receive）和关闭（close）三种操作。

发送和接收都使用`<-`符号。

只有在通知接收方goroutine所有的数据都发送完毕的时候才需要关闭通道。

通道是**可以被垃圾回收机制回收的**，它和关闭文件是不一样的，在结束操作之后关闭文件是必须要做的，

但**关闭通道不是必须的**。

* 对一个关闭的通道再发送值就会导致panic。
* 对一个关闭的通道进行接收会一直获取值直到通道为空。
* 对一个关闭的并且没有值的通道执行接收操作会得到对应类型的零值。
* 关闭一个已经关闭的通道会导致panic。

### Channel Demo

```go
func main() {
    //var ch chan int //引用类型 需要初始化才能使用
    //ch = make(chan int,1) //make初始化
    ch1 := make(chan int,2) //声明并初始化 缓存通道 2
    ch2 := make(chan int) //无缓冲区通道 又称同步通道 阻塞的通道

    ch1 <- 10	//发送
    x := <- ch1 // 从ch中接收值并赋值给变量x
    <-ch1       // 从ch中接收值，忽略结果
    fmt.Println(x)
    //len(ch1)	//通道中元素的数量
    //cap(ch1)	//通道的容量
    close(ch1)	//关闭
}
```

### 无缓冲的 channel 和 有缓冲的 channel 的区别？

对于无缓冲的 channel，发送方将阻塞该信道，直到接收方从该信道接收到数据为止，而接收方也将阻塞该信道，直到发送方将数据发送到该信道中为止。

对于有缓存的 channel，发送方在没有空插槽（缓冲区使用完）的情况下阻塞，而接收方在信道为空的情况下阻塞。

例如:

```go
func main() {
	st := time.Now()
	ch := make(chan bool)
	go func ()  {
		time.Sleep(time.Second * 2)
		<-ch
	}()
	ch <- true  // 无缓冲，发送方阻塞直到接收方接收到数据。
	fmt.Printf("cost %.1f s\n", time.Now().Sub(st).Seconds())
	time.Sleep(time.Second * 5)
}
```

```go
func main() {
	st := time.Now()
	ch := make(chan bool, 2)
	go func ()  {
		time.Sleep(time.Second * 2)
		<-ch
	}()
	ch <- true
	ch <- true // 缓冲区为 2，发送方不阻塞，继续往下执行
	fmt.Printf("cost %.1f s\n", time.Now().Sub(st).Seconds()) // cost 0.0 s
	ch <- true // 缓冲区使用完，发送方阻塞，2s 后接收方接收到数据，释放一个插槽，继续往下执行
	fmt.Printf("cost %.1f s\n", time.Now().Sub(st).Seconds()) // cost 2.0 s
	time.Sleep(time.Second * 5)
}
```

### channel实现(协程goroutine通信)

```go
//生成0~100的数字发送到ch1
func f1(ch chan<- int){
	for i := 0; i < 10; i++ {
		ch <- i
	}
	close(ch)
}
//从ch1中取出数据计算它的平方 把结果发送到ch2中
func f2(ch1 <-chan int,ch2 chan<- int){
	//从通道中取值的方式一
	for {
		tmp,ok := <- ch1
		if !ok {
			break
		}
		ch2<-tmp*tmp
	}
	close(ch2)
}
func main() {
	ch1 := make(chan int,100)
	ch2 := make(chan int,100)
	go f1(ch1)
	go f2(ch1,ch2)
	//从通道中取值的方式二
	for res := range ch2 {
		fmt.Println(res)
	}
}
```

#### 单向通道

`<-chan` 只读，只能接收的通道

`chan<-` 只写入，只能发送的通道

### select与channel

举个小例子来演示下`select`的使用：

```go
func main() {
	ch := make(chan int, 1)
	for i := 0; i < 10; i++ {
		select {
		case x := <-ch:
			fmt.Println(x)
		case ch <- i:
		}
	}
}
```

使用`select`语句能提高代码的可读性。

* 可处理一个或多个channel的发送/接收操作。
* 如果多个`case`同时满足，`select`会随机选择一个。
* 对于没有`case`的`select{}`会一直等待，可用于阻塞main函数。

#### channel超时机制(定时器)

golang 并没有为 channel 超时提供直接的解决方案，但我们可以利用 select 机制。虽然 select 机制不是专为超时而设计的，却能很方便地解决超时问题。

select 的特点是只要其中一个 case 已经完成，程序就会继续往下执行，而不会考虑其他 case 的情况。

```go
func main() {
	fmt.Println("开始")
	ch := make(chan int)
	quit := make(chan bool)
	//新开一个协程
	go func() {
		for {
			select {
			case num := <-ch:  //如果有数据，下面打印。但是有可能ch一直没数据
				fmt.Println("received num = ", num)
			case <-time.After(3 * time.Second): //上面的ch如果一直没数据会阻塞，那么select也会检测其他case条件，检测到后3秒超时
				fmt.Println("超时")
				quit <- true  //写入
			}
		}
	}() //注意() 运行这个匿名函数
	for i := 0; i < 3; i++ {
		ch <- i
		time.Sleep(time.Second) 
	}
	<-quit //这里暂时阻塞，直到可读
	fmt.Println("结束")
}
```

### `channel`常见的异常总结
![channel异常](https://images.gitee.com/uploads/images/2021/0918/110607_008d293a_9357316.png "channel异常.png")

关闭已经关闭的`channel`也会引发`panic`

## runtime协程切换与多核控制

## 多线程与线程安全

## 互斥锁与读写锁

### 互斥锁

> 互斥锁是一种常用的控制共享资源访问的方法，它能够保证同时只有一个`goroutine`可以访问共享资源。
>

Go语言中使用`sync`包的`Mutex`类型来实现互斥锁

```go
var (
	x int64
	wg sync.WaitGroup	//限制goroutine并行数
	lock sync.Mutex		//互斥锁
)

func add() {
	for i := 0; i < 5000; i++ {
		lock.Lock() // 加锁
		x = x + 1
		lock.Unlock() // 释放锁
	}
	wg.Done()
}
func main() {
	wg.Add(2)
	go add()
	go add()
	wg.Wait()
	fmt.Println(x)
}
```

1. 使用互斥锁能够保证同一时间有且只有一个`goroutine`进入临界区，其他的`goroutine`则在等待锁；
2. 当互斥锁释放后，等待的`goroutine`才可以获取锁进入临界区，
3. 多个`goroutine`同时等待一个锁时，唤醒的策略是随机的。

### 读写锁

互斥锁是完全互斥的，但是有很多实际的场景下是**读多写少**的，当我们并发的去读取一个资源不涉及资源修改的时候是没有必要加锁的，这种场景下使用读写锁是更好的一种选择。**读写锁在Go语言中使用sync包中的RWMutex类型**。

读写锁分为两种：读锁和写锁。

当一个`goroutine`获取读锁之后，其他的`goroutine`如果是获取读锁会继续获得锁，如果是获取写锁就会等待；

当一个`goroutine`获取写锁之后，其他的`goroutine`无论是获取读锁还是写锁都会等待。

```go
var (
	x      int64
	wg     sync.WaitGroup
	//lock   sync.Mutex	//互斥锁
	rwlock sync.RWMutex	//读写互斥锁
)

func read() {   
	rwlock.RLock()               // 加读锁
	time.Sleep(time.Millisecond) // 假设读操作耗时1毫秒
	rwlock.RUnlock()             // 解读锁
	wg.Done()
}

func write() {
	rwlock.Lock() // 加写锁
	x = x + 1
	time.Sleep(time.Millisecond*10) // 假设读操作耗时10毫秒
	rwlock.Unlock()                   // 解写锁
	wg.Done()
}

func main() {
	start := time.Now()

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go read() //读1000次
	}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go write() //写10次
	}

	wg.Wait()
	end := time.Now()
	fmt.Println(end.Sub(start))
}
```

注意：读写锁非常适合读多写少的场景，**如果读和写的操作差别不大，读写锁的优势就发挥不出来**。

## sync包

### sync.WaitGroup

["sync.WaitGroup的使用场景"](siyuan://blocks/20210917104629-kf3cx8b)

### sync.Once

> 在编程的很多场景下我们需要确保**某些操作在高并发的场景下只执行一次**，例如只加载一次配置文件、只关闭一次通道等。
>

Go语言中的`sync`包中提供了一个针对只执行一次场景的解决方案–`sync.Once`。

`sync.Once`只有一个`Do`方法，其签名如下：

```go
func (o *Once) Do(f func()) {} 
```

*备注：如果要执行的函数`f`需要传递参数就需要搭配闭包来使用。*

#### 加载配置文件示例

延迟一个开销很大的初始化操作到真正用到它的时候再执行是一个很好的实践。因为预先初始化一个变量（比如在init函数中完成初始化）会增加程序的启动耗时，而且有可能实际执行过程中这个变量没有用上，那么这个初始化操作就不是必须要做的。我们来看一个例子：

```go
var icons map[string]image.Image

func loadIcons() {
	icons = map[string]image.Image{
		"left":  loadIcon("left.png"),
		"up":    loadIcon("up.png"),
		"right": loadIcon("right.png"),
		"down":  loadIcon("down.png"),
	}
}

// Icon 被多个goroutine调用时不是并发安全的
func Icon(name string) image.Image {
	if icons == nil { //loadIcons()还没执行过
		loadIcons()
	}
	return icons[name]
}
```

多个`goroutine`并发调用Icon函数时**不是并发安全的**，现代的编译器和CPU可能会在保证每个`goroutine`都满足串行一致的基础上自由地重排访问内存的顺序。

loadIcons函数可能会被重排为以下结果：

```go
func loadIcons() {
	icons = make(map[string]image.Image)
	icons["left"] = loadIcon("left.png") 
	//一个goroutine执行到此 
	//另一个goroutine判断icons != nil icons[name]取不到值
	icons["up"] = loadIcon("up.png")
	icons["right"] = loadIcon("right.png")
	icons["down"] = loadIcon("down.png")
}
```

在这种情况下就会出现即使判断了`icons`不是nil也不意味着变量初始化完成了。

考虑到这种情况，我们能想到的办法就是添加互斥锁，保证初始化`icons`的时候不会被其他的`goroutine`操作，但是**这样做又会引发性能问题**。

使用`sync.Once`改造的示例代码如下：

```go
var icons map[string]image.Image

var loadIconsOnce sync.Once

func loadIcons() {
	icons = map[string]image.Image{
		"left":  loadIcon("left.png"),
		"up":    loadIcon("up.png"),
		"right": loadIcon("right.png"),
		"down":  loadIcon("down.png"),
	}
}

// Icon 是并发安全的
func Icon(name string) image.Image {
	//判断loadIcons有没有执行过 没有就执行一次
	loadIconsOnce.Do(loadIcons) 
	return icons[name]
}
```

#### 并发安全的单例模式

下面是借助`sync.Once`实现的并发安全的单例模式：

```go
package singleton

import (
    "sync"
)

type singleton struct {}

var instance *singleton
var once sync.Once

func GetInstance() *singleton {
    once.Do(func() {
        instance = &singleton{}
    })
    return instance
}
```

`sync.Once`其实内部包含一个互斥锁和一个布尔值，互斥锁保证布尔值和数据的安全，而布尔值用来记录初始化是否完成。这样设计就能保证初始化操作的时候是并发安全的并且初始化操作也不会被执行多次。

### sync.Map

Go语言中**内置的map不是并发安全的**。请看下面的示例：

```go
var m = make(map[string]int)

func get(key string) int {	// 
	return m[key]
}

func set(key string, value int) {
	m[key] = value
}

func main() {
	wg := sync.WaitGroup{}
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func(n int) {
			key := strconv.Itoa(n)
			set(key, n)
			fmt.Printf("k=:%v,v:=%v\n", key, get(key))
			wg.Done()
		}(i)
	}
	wg.Wait()
}
```

上面的代码开启少量几个`goroutine`的时候可能没什么问题，当并发多了之后执行上面的代码就会报`fatal error: concurrent map writes`错误。

像这种场景下就**需要为map加锁来保证并发的安全性**了

Go语言的`sync`包中提供了一个开箱即用的并发安全版map–`sync.Map`。

**开箱即用**表示不用像内置的map一样使用make函数初始化就能直接使用。

同时`sync.Map`内置了诸如`Store`、`Load`、`LoadOrStore`、`Delete`、`Range`等操作方法。

```go
var m = sync.Map{}

func main() {
	wg := sync.WaitGroup{}
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func(n int) {
			key := strconv.Itoa(n)
			m.Store(key, n) //存键值对
			value, _ := m.Load(key) //获取值
			fmt.Printf("k=:%v,v:=%v\n", key, value)
			wg.Done()
		}(i)
	}
	wg.Wait()
}
```

### 原子操作 sync/atomic

在上面的代码中的我们通过锁操作来实现同步。

而锁机制的底层是基于原子操作的，其一般直接通过CPU指令实现。Go语言中原子操作由内置的标准库`sync/atomic`提供。

#### atomic包

只提供了一些基本数据类型的

| 方法                                                                                                                                                                                                                                                                                                                                                                                                                                                     |      解释      |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------: |
| func LoadInt32(addr *int32) (val int32)<br />func LoadInt64(addr *int64) (val int64)<br />func LoadUint32(addr *uint32) (val uint32)<br />func LoadUint64(addr *uint64) (val uint64)<br />func LoadUintptr(addr *uintptr) (val uintptr)<br />func LoadPointer(addr *unsafe.Pointer) (val unsafe.Pointer)                                                                                                                                                                               |    读取操作    |
| func StoreInt32(addr *int32, val int32)<br />func StoreInt64(addr *int64, val int64)<br />func StoreUint32(addr *uint32, val uint32)<br />func StoreUint64(addr *uint64, val uint64)<br />func StoreUintptr(addr *uintptr, val uintptr)<br />func StorePointer(addr *unsafe.Pointer, val unsafe.Pointer)                                                                                                                                                                               |    写入操作    |
| func AddInt32(addr *int32, delta int32) (new int32)<br />func AddInt64(addr *int64, delta int64) (new int64)<br />func AddUint32(addr *uint32, delta uint32) (new uint32)<br />func AddUint64(addr *uint64, delta uint64) (new uint64)<br />func AddUintptr(addr *uintptr, delta uintptr) (new uintptr)                                                                                                                                                                          |    修改操作    |
| func SwapInt32(addr *int32, new int32) (old int32)<br />func SwapInt64(addr *int64, new int64) (old int64)<br />func SwapUint32(addr *uint32, new uint32) (old uint32)<br />func SwapUint64(addr *uint64, new uint64) (old uint64)<br />func SwapUintptr(addr *uintptr, new uintptr) (old uintptr)<br />func SwapPointer(addr *unsafe.Pointer, new unsafe.Pointer) (old unsafe.Pointer)                                                                                                |    交换操作    |
| func CompareAndSwapInt32(addr *int32, old, new int32) (swapped bool)<br />func CompareAndSwapInt64(addr *int64, old, new int64) (swapped bool)<br />func CompareAndSwapUint32(addr *uint32, old, new uint32) (swapped bool)<br />func CompareAndSwapUint64(addr *uint64, old, new uint64) (swapped bool)<br />func CompareAndSwapUintptr(addr *uintptr, old, new uintptr) (swapped bool)<br />func CompareAndSwapPointer(addr *unsafe.Pointer, old, new unsafe.Pointer) (swapped bool) | 比较并交换操作 |

#### **示例**

比较下互斥锁和原子操作的性能。

```go
package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

type Counter interface {
	Inc()
	Load() int64
}

// 普通版
type CommonCounter struct {
	counter int64
}

func (c CommonCounter) Inc() {
	c.counter++
}

func (c CommonCounter) Load() int64 {
	return c.counter
}

// 互斥锁版
type MutexCounter struct {
	counter int64
	lock    sync.Mutex
}

func (m *MutexCounter) Inc() {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.counter++
}

func (m *MutexCounter) Load() int64 {
	m.lock.Lock()
	defer m.lock.Unlock()
	return m.counter
}

// 原子操作版
type AtomicCounter struct {
	counter int64
}

func (a *AtomicCounter) Inc() {
	atomic.AddInt64(&a.counter, 1)
}

func (a *AtomicCounter) Load() int64 {
	return atomic.LoadInt64(&a.counter)
}

func test(c Counter) {
	var wg sync.WaitGroup
	start := time.Now()
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func() {
			c.Inc()
			wg.Done()
		}()
	}
	wg.Wait()
	end := time.Now()
	fmt.Println(c.Load(), end.Sub(start))
}

func main() {
	c1 := CommonCounter{} // 非并发安全
	test(c1)
	c2 := MutexCounter{} // 使用互斥锁实现并发安全
	test(&c2)
	c3 := AtomicCounter{} // 并发安全且比互斥锁效率更高
	test(&c3)
}
```

`atomic`包提供了底层的原子级内存操作，对于同步算法的实现很有用。这些函数必须谨慎地保证正确使用。除了某些特殊的底层应用，使用通道或者sync包的函数/类型实现同步更好。

## 什么是协程泄露(Goroutine Leak)？

协程泄露是指协程创建后，长时间得不到释放，并且还在不断地创建新的协程，最终导致内存耗尽，程序崩溃。常见的导致协程泄露的场景有以下几种：

* **缺少接收器，导致发送阻塞**

这个例子中，每执行一次 query，则启动1000个协程向信道 ch 发送数字 0，但只接收了一次，导致 999 个协程被阻塞，不能退出。

```go
func query() int {
	ch := make(chan int)
	for i := 0; i < 1000; i++ {
		go func() { ch <- 0 }()
	}
	return <-ch
}

func main() {
	for i := 0; i < 4; i++ {
		query()
		fmt.Printf("goroutines: %d\n", runtime.NumGoroutine())
	}
}
// goroutines: 1001
// goroutines: 2000
// goroutines: 2999
// goroutines: 3998
```

* **缺少发送器，导致接收阻塞**

那同样的，如果启动 1000 个协程接收信道的信息，但信道并不会发送那么多次的信息，也会导致接收协程被阻塞，不能退出。

* **死锁(dead lock)**

两个或两个以上的协程在执行过程中，由于竞争资源或者由于彼此通信而造成阻塞，这种情况下，也会导致协程被阻塞，不能退出。

* **无限循环(infinite loops)**

这个例子中，为了避免网络等问题，采用了无限重试的方式，发送 HTTP 请求，直到获取到数据。那如果 HTTP 服务宕机，永远不可达，导致协程不能退出，发生泄漏。

```go
func request(url string, wg *sync.WaitGroup) {
	i := 0
	for {
		if _, err := http.Get(url); err == nil {
			// write to db
			break
		}
		i++
		if i >= 3 {
			break
		}
		time.Sleep(time.Second)
	}
	wg.Done()
}

func main() {
	var wg sync.WaitGroup
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go request(fmt.Sprintf("https://127.0.0.1:8080/%d", i), &wg)
	}
	wg.Wait()
}
```

## 说出一个避免Goroutine泄露的措施

### 通过 context 包来避免内存泄漏。

```go
func main() {
    ctx, cancel := context.WithCancel(context.Background())

    ch := func(ctx context.Context) <-chan int {
        ch := make(chan int)
        go func() {
            for i := 0; ; i++ {
                select {
                case <- ctx.Done():
                    return
                case ch <- i:
                }
            }
        } ()
        return ch
    }(ctx)

    for v := range ch {
        fmt.Println(v)
        if v == 5 {
            cancel()
            break
        }
    }
}
```

下面的 for 循环停止取数据时，就用 cancel 函数，让另一个协程停止写数据。如果下面 for 已停止读取数据，上面 for 循环还在写入，就会造成内存泄漏。

### worker pool（goroutine池）

在工作中我们通常会使用可以指定启动的goroutine数量–`worker pool`模式，控制`goroutine`的数量，防止`goroutine`泄漏和暴涨。

一个简易的`work pool`示例代码如下：

```go
func worker(id int, jobs <-chan int, results chan<- int) {
	for job := range jobs {
		//id（哪个goroutine） jos（做了什么工作）
		fmt.Printf("worker:%d start job:%d\n", id, job)
		results <- job * 2
		time.Sleep(time.Second)//等待1秒
		fmt.Printf("worker:%d end job:%d\n", id, job)
	}
}

func main() {
	jobs := make(chan int, 100)
	results := make(chan int, 100)
	// 开启3个goroutine
	for w := 0; w < 3; w++ {
		go worker(w, jobs, results)
	}
	// 5个任务
	for j := 0; j < 5; j++ {
		jobs <- j
	}
	close(jobs)
	// 输出结果
	for a := 0; a < 5; a++ {
		res := <-results
		fmt.Println(res)
	}
}
```