# go

#### 优势
![编译速度最快](https://images.gitee.com/uploads/images/2021/0726/142749_3f671ce9_9357316.png "编译速度最快.png")
![强项](https://images.gitee.com/uploads/images/2021/0726/142802_abe1183c_9357316.png "强项.png")
#### go函数
1. 函数介绍
2. 函数变量
3. 字符串的链式处理
4. 匿名函数、闭包函数
5. defer语句特性
6. go计算函数执行时间
#### go结构体
1. 结构体定义
2. 结构体内存布局
3. 构造函数
4. 匿名字段和嵌套struct
5. 指针类型
6. 面向对象与继承
7. type func
#### go接口
1. 接口介绍与定义
2. 接口嵌套
3. 类型分支
4. 空接口与type switch
5. 接口内部实现
#### go语言包
1. 包的概念
2. 导入包
3. 自定义包
4. 工程模式自动注册
5. 单例模式
6. mod包依赖管理工具
#### go反射
1. 反射(reflection) 
2. 反射规则浅析
3. 反射基本操作
4. 反射与结构体
5. 结构与动态类型
#### go文件处理
1. 自定义数据文件
2. json、xml、纯文本等读写操作
3. 使用gob传输数据
4. 使用buffer读取文件
5. 文件的操作
6. 文件锁操作
#### 并发编程
[go 并发编程](https://gitee.com/mingkong112/go/tree/master/go%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E7%82%B9/%E5%B9%B6%E5%8F%91%E7%BC%96%E7%A8%8B)
#### 网络编程
[go 网络编程](https://gitee.com/mingkong112/go/tree/master/go%E5%9F%BA%E7%A1%80%E7%9F%A5%E8%AF%86%E7%82%B9/%E7%BD%91%E7%BB%9C%E7%BC%96%E7%A8%8B)
#### 关键字
##### import
1. 导入包<br>
```go
//单个包
import "fmt"  
//多个包 
import (  
// fmt.Printf()
    "fmt"              
// 导入不使用 _ 无法使用包的方法 会执行包内部的init()方法
    _ "time"  
// 起别名 aa.Printf()直接调用
   aa "fmt"   
// 加.可直接Printf() 最好不要使用避免多个包有同名方法
    . "fmt"         
// %GOPATH/自建包地址
    "aaa/bbb/lib"    
)
```
2. Init()函数和import导包调用过程<br>
> ![import导包调用过程](https://images.gitee.com/uploads/images/2021/0726/152940_965364f1_9357316.png "import导包调用过程.png")
##### var(变量)
> 定义变量<br>
1. 声明单个变量
```go
//系统自动赋予它该类型的零值
//eg：int 为 0，float 为 0.0，bool 为 false，string 为空字符串，指针为 nil 等    
//方法一 不赋值 默认值为0    
var a int   
//方法二 初始化一个值 
var b int = 100  
//方法三 通过值自动匹配当前变量的数据类型
var c = "abcd"   
//方法四(常用) 省去var关键字 自动匹配变量类型 (不能声明全局变量)
d := 100
```
2. 声明多个变量
```go
//单行变量声明
var aa,bb int = 100  
var aa,bb = 100,"hello"  
//多行变量声明  
var (  
    aa int = 100  
    bb bool = true  
)       
```
##### const(常量)
> 定义常量<br>
1. 常量**const**具有只读属性<br>  
```go
const length int = 10  
const {  
    a = 10  
    b = 20  
}  
length = 100 //错误 常量不允许修改 
```
2. 通过**const**来定义枚举类型<br>
```go
const (  
//const()可添加关键字iota,每行都会累加1,第一行iota默认为0  
//iota只能配合const()一起使用  
    suzhou = 0+iota    //iota=0  
    hangzhou           //iota=1  
    nanjing            //iota=2  

    a,b = iota+1,iota+2    //iota=0 a=1 b=2  
    c,d                    //iota=1 c=2 d=3  
    e,f                    //iota=2 e=3 f=4  
    g,h = iota*2,iota*3    //iota=3 g=6 h=9  
    i,j                    //iota=4 i=8 j=12     
)
```
##### type(定义类型) 
1. type 声明一种数据类型  
```go
type myint int    //var a myint = 10 
```
2. type 定义一个结构体 封装  
```go
//类名首字母大写 表示其他包也可以访问该定义对象  
type Book struct {  
    //首字母大写 该属性对外能够访问 否则只能内部访问  
    Title  string  
    auth string  
}
```
##### struct(结构体)
1. 创建结构体<br>
```go
func main() {  
    //创建结构体对象 1  
    var book1 Book  
    book1.title = "语文"  
    book1.auth = "张三"  
    fmt.Printf("%v\n", book1) //{语文 张三}  
    changeBook1(book1)  
    fmt.Printf("%v\n", book1) //{语文 张三}  
    changeBook2(&book1)  
    fmt.Printf("%v\n", book1) //{语文 王五}  
    //创建结构体对象 2  
    book2 := {title: "语文" , auth : "张三"}  
    book2.ShowAuth()  
    book2.SetAuth("王五")  
    fmt.Println(book2.GetAuth()) //张三  
}
```
2. 操作结构体
```go
func changeBook1(book Book){  
    //传递一个book的副本  
    book.auth = "李四"  
}  
func changeBook2(book *Book){  
    //指针传递  
    book.auth = "王五"  
}  
func (this *Book) ShowAuth() {  
    fmt.Println("auth = ", this.auth)  
}  
func (this *Book) getAuth() string {  
    return this.auth  
}  
func (this *Book) setAuth(newAuth string) {  
    this.Auth = newAuth  
}
```
##### interface(接口)
> 接口定义<br>
```go
//interface{}万能数据类型 
//可以⽤interface{}类型引⽤任意的数据类型
func myFunc(arg interface{}) {
	fmt.Println("myFunc is called...")
	fmt.Println(arg)
	//interface{} 提供"数据断言"的机制
	//可用来区分此时引用的底层数据类型到底是什么
	value, ok := arg.(string)
	if !ok {
		fmt.Println("arg is not string type")
	} else {
		fmt.Println("arg is string type, value = ", value)
		fmt.Printf("value type is %T\n", value)
	}
}
```
##### func
> 函数定义<br>
```go
func main(){   
    c := foo1("aaa",100)  
    fmt.Println("c = ",c)    //c=100  
  
    ret1,ret2 := foo2("aaa",100)  
    fmt.Println("ret1 = ", r1 ,"ret2 = ", r2)    //ret1=111,ret2=222
    
    ret1,ret2 := foo3("aaa",100)  
    fmt.Println("ret1 = ", r1 ,"ret2 = ", r2)    //ret1=1000,ret2=2000
}
```
1. 一个返回值 
```go
func foo1 (a string,b int) int {  
    fmt.Println("a = ", a)  
    fmt.Println("b = ", b)  
    c := 100      
    return  
}
```
2. 多个返回值(匿名)  
```go
func foo2 (a string,b int) (int, int) {  
    fmt.Println("a = ", a)  
    fmt.Println("b = ", b)  
    return 111,222  
}  
```
3. 多个返回值(有形参名称) 
```go
//r1 r2相同返回值同种类型可简写 放一起  
//func foo3 (a string,b int) (r1 int, r2 int)
func foo3 (a string,b int) (r1 , r2 int) {  
    fmt.Println("a = ", a)  
    fmt.Println("b = ", b)  
    
    //r1 r2 属于形参初始化默认值为0  
    //作用域是foo3整个函数体的空间  
    fmt.Println("r1 = ",r1)    // r1 = 0  
    //给有名的返回值变量赋值  
    r1 = 1000  
    r2 = 2000  
    return  
}  
```
##### return
> 返回<br>
> 
##### defer
> 延迟执行内容（收尾工作）有点类似C++的析构，但是它是再函数结尾的时候去执行（也就是栈即将被释放的时候）<br>
> ![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/153859_614c02ae_9357316.png "屏幕截图.png")<br>
> ![defer执⾏顺序](https://images.gitee.com/uploads/images/2021/0726/143819_32b172f5_9357316.png "defer执⾏顺序.png")<br>
> return后的语句先执行 defer后的语句后执行
##### default
> 用于选择结构的默认选项（switch、select）<br>
>
##### select
> go语言特有的channel选择结构<br>
>
##### case
> 选择结构标签<br>
>
##### go
> 并发执行<br>
>
##### map
> map类型 map传参是一个引用传递 可以直接进行修改<br>
###### map声明
1. 声明myMap1是一个map类型 
```go
    //声明myMap1 key是string value是string 
    //make分配数据空间  
    var myMap1 map[string]string = make(map[string]string, 10)
```
2. 空间10可以不写 即 动态 
```go
    myMap2 := make(map[string]string)  
    myMap2["1"] = "a"  
    myMap2["2"] = "b"  
    myMap2["3"] = "c"  
    fmt.Println(myMap2) // map[1:a 2:b 3:c] 
```
3. 直接定义数据  
```go
    myMap3 := map[string]string {  
        "4": "d"  
        "5": "e"  
        "6": "f"  
    }  
    fmt.Println(myMap3) // map[4:d 5:e 6:f]  
```
###### map操作
```go
    //map增加  
    myMap2["4"] = "d"  
    //map删除  
    delete(myMap2,"1")   
    //map遍历  
    for key,value := range myMap2 {  
    fmt.Println("key = ", key)  
    fmt.Println("value = ", value)  
    }  
    //map修改  
    myMap2["2"] = "d"   
```
##### chan
> 定义channel<br>
```go
func main() {
    //定义一个channel
    c := make(chan int)
    go func() {
        defer fmt.Println("goroutine结束")
        fmt.Println("goroutine 运行中。。。")
        c <- 666 //将666发送给c
    }
    num := <-c
    fmt.Println("num = ", num)
    fmt.Println("main goroutine 结束。。。")
}
```
##### else
> 选择结构<br>
>
##### goto
> 跳转语句<br>
>
##### package
> 包<br>
>
##### switch
> 选择结构<br>
>
##### fallthrough
> 如果case带有fallthrough，程序会继续执行下一条case,不会再判断下一条case的值<br>
> 
##### if
> 选择结构<br>
> 
##### range
> 从slice、map等结构中取元素<br>
> 
##### for
> 循环<br>
> 
##### break
> 跳出循环<br>
> break语句可以结束 for、switch 和 select 的代码块<br>
> break语句还可以在语句后面添加标签，表示退出某个标签对应的代码块，标签要求必须定义在对应的 for、switch 和 select 的代码块上<br>
```go
/*输出：
0 2
*/
func main() {
OuterLoop:
	for i := 0; i < 2; i++ {
		for j := 0; j < 5; j++ {
			switch j {
			case 2:
				fmt.Println(i, j)
				break OuterLoop
			case 3:
				fmt.Println(i, j)
				//break OuterLoop
			}
		}
	}
}
```
##### continue
> 跳过本次循环<br>
> 结束当前循环，开始下一次的循环迭代过程，仅限在 for 循环内使用<br>
> 在continue语句后添加标签时，表示开始标签对应的循环<br>
```go
/*输出：
0 2
1 2
*/
func main() {
OuterLoop:
	for i := 0; i < 2; i++ {
		for j := 0; j < 5; j++ {
			switch j {
			case 2:
				fmt.Println(i, j)
				continue OuterLoop
			}
		}
	}
}
```

#### 指针
> 
#### 数组
```go
func main(){  
    //数组 固定长度  
    var myArray1 [10]int  
    var myArray2 [10]int{1,2,3,4}    
    //动态数组 不固定长度  
    myArray := []int{1,2,3,4}  
    //数组遍历  
    printArray(myArray)  
} 
```
1. 遍历固定数组
```go
//[4]int即只能遍历长度为4的数组 (一般不用)
func printArray(myArray [4]int){   
    //值拷贝  
    for index,value range myArray{  
        fmt.println("index = ", index, "value = ", value)  
    }  
    myArray[0] = 111 //原myArray[0]的值不变  
}
```
2. 遍历动态数组
> 动态数组即切片slice<br>
```go
//可改变长度值 传递的是当前数组的指针  
func printArray(myArray []int){  
    //引用传递  
    for index,value range myArray{  
    fmt.println("index = ", index, "value = ", value)  
    }  
    // _ 表示匿名的变量 不使用  
    for _,value range myArray{  
        fmt.println("value = ", value)  
    }  
    myArray[0] = 111 // 原myArray[0]的值可以改变  
}  
```
#### 切片(slice)
> 动态数组即切片(slice)<br>
##### 声明切片
```go
func main(){  
    //slice切片  
    //方式一 声明一个slice并初始化 默认值1，2，3 长度len = 3  
    slice1 := []int{1,2,3}   
    //方式二 声明slice1是一个切片 但没有给slice分配空间  
    var slice2 []int  
    //======错误 slice2没有空间   
    slice2[0] = 100   
    //======开辟三个空间 默认值为0 可以赋值  
    slice2 = make([]int, 3)   
    //方式三 四 常用第四种  
    var slice3 []int = make([]int,3)  
    slice3 := make([]int, 3)  
    //判断一个slice是否为空  
    if slice3 == nil {  
        fmt.Println("slice3 是一个空切片")  
    }else {  
        fmt.Println("slice3 有空间容量")  
    }  
    //切片 len=3 cap=5  
    s := []int{1,2,3}  
    //切片追加一个元素 三个元素  
    ss = append(s, 1) //len=4 cap=5  
    ss = append(s, 3) //len=6 cap=10  
    //切片截取  
    s1 := s[0:2]          //从0开始截取两个  
    s1[0] = 100  
    //s1和s 指向同一个空间  
    fmt.Println("s1")  //[100 2]  
    fmt.Println("s")    //[100 2 3]  
      
    //copy 可以将底层数组的slice一起进行拷贝  
    s2 := make([]int, 3)  
    copy(s2, s) //将s中的值 依次拷贝到s2中  改变s2 s不变  
    s2[0] = 1000  
    fmt.Println("s2")    //[1000 2 3]  
    fmt.Println("s")      //[100 2 3]  
}  
```
##### 容量追加
```go
Slice1 := make([]int, 3 , 5)  
Len = 3 cap = 5  
Slice1 = append(Slice1 ,1)  
Len = 4 cap = 5  
Slice1 = append(Slice1 ,3)  
Len = 5 cap = 10  
```
###### 切片扩容机制
> 切⽚的⻓度和容量不同，⻓度表示左指针⾄右指针之间的距离，容量表示左指针⾄底层数组末尾的距离.<br>
> append的时候，如果⻓度增加后超过容量，则将容量增加2倍.<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/160514_e329e660_9357316.png "屏幕截图.png")<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/160518_e4e17d96_9357316.png "屏幕截图.png")<br>
##### 切片截取
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/160732_2174d690_9357316.png "屏幕截图.png")
#### 反射(reflect)
```go
//反射 reflect包  
import "reflect"  
type User struct {  
    Id int  
    Name string  
    Age int  
}  
func (this *User) Call() {  
    fmt.Println("user is called . . .")  
    fmt.Println("%v\n", this)  
}  
func main() {  
    var num float64 =1.2345  
    fmt.Println("type : ", reflect.TypeOf(num))//type:float64 
    fmt.Println("value : ", reflect.ValueOf(num))//value:1.234
    //复杂类型 Type: User Value: {1, "张三", 22}  
    user := User{1, "张三", 22}  
    DoFiledAndMethod(uesr)  
}
```
```go
func DoFiledAndMethod(input interface{}) {  
    //获取input的type Tyep is User  
    inputType := reflect.TypeOf(input)  
    fmt.Println("inputType is : ", inputType.Name())  
    //获取input的value Value is {1 张三 22}  
    inputType := reflect.ValueOf(input)  
    fmt.Println("inputVaule is : ", inputValue)  
    //通过type 获取里面的字段  
    //1. 获取interface的reflect.type, 通过type得到NumField，进行遍历  
    for i := 0; i< inputType.NumField(); i++ {  
        //2. 得到每个field，数据类型  
        field := inputType.Field(i)  
        //3. 通过filed有一个 Interface() 方法得到对应的 value  
        value := inputType.Field(i).Interface()  
        /*%s: %v = %v 
          Id: int = 1 
          Name: string = 张三 
          Age: int = 22*/  
        fmt.Printf("%s: %v = %v\n", field.Name, field.Type, value)  
    }   
    //通过type 获取里面的方法 调用  
    for i := 0; i < inputType.NumMethod(); i++ {  
        m := inputType.Method(i)  
        // Call: func(main.User)  
        fmt.Printf("%s: %v\n", m.Name, m.Type)  
    }   
} 
```
##### 解析结构体标签
```go
type book struct {  
    Title string `info:"title" doc:"标题"`  
    Auth string `info:"auth"`  
}  
func findTag(str interface{}) {  
    //当前结构体全部元素  
    t := reflect.TypeOf(str).Elem()  
 
    for i := 0; i < t.NumField(); i++ {  
        //已知标签名称  
        taginfo := t.Field(i).Tag.Get("info")  
        tagdoc := t.Field(i).Tag.Get("info")  
        fmt.Println("info: ", tagstring)  
        fmt.Println("doc: "tagdoc)  
    }  
}  
func main() {  
    var ww book  
    // info: title doc: 标题  
    // info: auth doc:       
    fingTag(&ww)  
}
```
#### Json 结构体 互转
```go
type Book struct {  
    Title []string `json:"title"`  
    Auth string `json:"auth"`  
}  
func main() {  
    book := Book{[]string{"张三","王五"},"不想早起"}  
    //编码过程 结构体-->json  
    jsonStr, err := json.Marshal(book)  
    if err != nil {  
        fmt.Println("json marshal error", err)  
        return  
    }  
    fmt.Printf("jsonStr = %s\n", jsonStr)  
    //解码 json-->结构体  
    myBook := Book{}  
    err = json.Unmarshal(jsonStr, &myBook)  
    if err != nil {  
        fmt.Println("json unmarshal error", err)  
        return  
    }  
    fmt.Printf("myBook = %s\n", myBook )  
}
```
#### string int 类型相转
```go
//string转成int：
int, err := strconv.Atoi(string)
//string转成int64：
int64, err := strconv.ParseInt(string, 10, 64)
//int转成string：
string := strconv.Itoa(int)
//int64转成string：
string := strconv.FormatInt(int64,10)
```
#### string float 互转
```go
//1. float到string
string := strconv.FormatFloat(float32,'E',-1,32)
string := strconv.FormatFloat(float64,'E',-1,64)
// 'b' (-ddddp±ddd，二进制指数)
// 'e' (-d.dddde±dd，十进制指数)
// 'E' (-d.ddddE±dd，十进制指数)
// 'f' (-ddd.dddd，没有指数)
// 'g' ('e':大指数，'f':其它情况)
// 'G' ('E':大指数，'f':其它情况)

//2. string到float64
float,err := strconv.ParseFloat(string,64)

//3. string到float32
float,err := strconv.ParseFloat(string,32)
```